﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KutuYokEtmeSC : MonoBehaviour
{
   
    private void OnTriggerEnter(Collider other)
    {
        if (this.transform.tag == other.tag)
        {
            ScoreScript.score++;
            PlanetScript.speed += 1f;//ayarlanır
            if (FurnitureSpawner.timeBetweenSpawns > 1f) //minimum spawn aralıgı
            {
                FurnitureSpawner.timeBetweenSpawns -= 0.05f;
            }

            #region Spawn Meteor
            if (ScoreScript.score%20==0)
            {
                FurnitureSpawner.timeBetweenSpawns -= 0.1f;
            }
            #endregion
            Destroy(other.gameObject);
            Debug.Log("Kutu sağ-sol yok edildi");
        }
        else
        {
            PlanetScript.speed += 3f;//ayarlanır tmm kardeşim
            Destroy(other.gameObject);

        }
    }
}
