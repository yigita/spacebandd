﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseSC : MonoBehaviour
{
    bool pause = false;

    public void pauseGame() 
    {
        if (pause)
        {
            Time.timeScale = 1;
            pause = false;
        }
        else
        {
            Time.timeScale = 0;
            pause = true;
        }
    }
}
