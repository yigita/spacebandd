﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class OyunKontrol : MonoBehaviour
{
    public Text UI_Point;
    public GameObject TappedMaterial;
    public int Point = 0;

    private Vector2 fingerStart;
    private Vector2 fingerEnd;

    public static List<GameObject> list = new List<GameObject>();


    public static void AddMateryal(GameObject go)
    {
        list.Add(go);
    }
    public static void RemoveMateryal(GameObject go)
    {
        list.Remove(go);
    }
    public static GameObject GetLastMateryal()
    {
        return list.Last();
    }
    public enum Movement
    {
        Left,
        Right,
        Up,
        Down
    };

    public List<Movement> movements = new List<Movement>();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UI_Point.text= string.Format($"{Point}");
        CheckTouchControl();
    }

    private void CheckTouchControl()
    {
        foreach (Touch touch in Input.touches)
        {

            if (touch.phase == TouchPhase.Began)
            {
                fingerStart = touch.position;
                fingerEnd = touch.position;

                //var newDokunmaCarpismaci = Instantiate(tiklananCarpismaci, tiklananCarpismaci.transform);


                //var v3 = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.y,0, touch.position.x));                
                //v3.y = tiklananCarpismaci.transform.position.y;
                //tiklananCarpismaci.transform.position = v3;


                Debug.Log($"Tıklandı..2D:{touch.position}, 3D:");
                OnTapStart();
            }

            if (touch.phase == TouchPhase.Moved)
            {
                fingerEnd = touch.position;

                //There is more movement on the X axis than the Y axis
                if (Mathf.Abs(fingerStart.x - fingerEnd.x) > Mathf.Abs(fingerStart.y - fingerEnd.y))
                {

                    //Right Swipe
                    if ((fingerEnd.x - fingerStart.x) > 0)
                        movements.Add(Movement.Right);
                    //Left Swipe
                    else
                        movements.Add(Movement.Left);
                    OnFingerMoveX(movements.Last());

                }

                //More movement along the Y axis than the X axis
                else
                {
                    //Upward Swipe
                    if ((fingerEnd.y - fingerStart.y) > 0)
                        movements.Add(Movement.Up);
                    //Downward Swipe
                    else
                        movements.Add(Movement.Down);
                }
                //After the checks are performed, set the fingerStart & fingerEnd to be the same
                fingerStart = touch.position;

                //Now let's check if the Movement pattern is what we want
                //In this example, I'm checking whether the pattern is Left, then Right, then Left again
                //Debug.Log(CheckForPatternMove(0, 3, new List<Movement>() { Movement.Left, Movement.Right, Movement.Left }));
                //btnOyunTestText.text = string.Format("MA={0}", string.Join(" ", movements.Select(o => o.ToString())));
                if (movements.Contains(Movement.Right))
                {
                    var item = GetLastMateryal();
                    if (item != null)
                    {
                        item.GetComponentInChildren<Renderer>().material.color = new Color(1, 1, 1, 0.3F);
                    }
                }
            }


            if (touch.phase == TouchPhase.Ended)
            {
                fingerStart = Vector2.zero;
                fingerEnd = Vector2.zero;
                movements.Clear();
                //


            }

        }
    }

    private void OnFingerMoveX(Movement movement)
    {
        if (TappedMaterial == null)
            return;

        var inc = 0.75F;
        inc = movement == Movement.Left ? inc * -1 : inc * 1;
        TappedMaterial.transform.position = new Vector3(TappedMaterial.transform.position.x, TappedMaterial.transform.position.y, TappedMaterial.transform.position.z + inc);
        Point += 1;
    }

    private void OnTapStart()
    {
        var raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        if (Physics.Raycast(raycast, out RaycastHit raycastHit))
        {
            var tappedItem = raycastHit.collider.gameObject;
            Debug.Log($"Carpilan object:{raycastHit.collider.name}");
            if (!tappedItem.name.StartsWith("Sag") && !tappedItem.name.StartsWith("Sol"))
                return;
            OnTapMaterial(tappedItem);

        }
    }

    private void OnTapMaterial(GameObject tappedItem)
    {
        TappedMaterial = tappedItem;
        TappedMaterial.transform.GetComponentInChildren<Renderer>().material.color = new Color(1, 0, 0);
    }
}