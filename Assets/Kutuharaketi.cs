﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kutuharaketi : MonoBehaviour
{
    public float speed;

    void FixUpdate()
    {
        transform.position = new Vector3(speed * Time.deltaTime, 0, 0);
    }
}
